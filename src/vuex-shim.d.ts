import { ComponentCustomProperties } from "vue";
import { Store } from "vuex";

declare module "@vue/runtime-core" {
  // Declare your own store states.
  interface State {
    user: {
      username: null;
      lastname: null;
      isAuth: false;
    }
  }

  interface ComponentCustomProperties {
    $store: Store<State>;
  }
}
