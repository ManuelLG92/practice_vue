import { createStore } from "vuex";

const store = createStore({
  state: {
    user: {
      name: null,
      lastname: null,
      isAuth: false,
    },
  },




  

  mutations: {
    setUserAuth(state, user) {
      Object.assign(state.user, user);
      //state.user = user
    },
  },
  getters: {
    getUser: (state) => {
      return state.user;
    },
  },
  actions: {},
  modules: {},
});
export default store
